000100**************************************************************/   51281408
000200*  PROGRAM: PPWWUBRC                                         */   51281408
000300*  RELEASE # ___1408___   SERVICE REQUEST NO(S)____15128_____*/   51281408
000400*  NAME __AAC__________   MODIFICATION DATE ____04/19/02_____*/   51281408
000500*  DESCRIPTION                                               */   51281408
000600*      STORED PROCEDURE TO RETURN A BRSC FOR A GIVEN BENEFIT */   51281408
000700*      PLAN                                                  */   51281408
000800*                                                            */   51281408
000900**************************************************************/   51281408
001000 IDENTIFICATION DIVISION.                                         PPWWUBRC
001100 PROGRAM-ID. PPWWUBRC.                                            PPWWUBRC
001200 ENVIRONMENT DIVISION.                                            PPWWUBRC
001300 CONFIGURATION SECTION.                                           PPWWUBRC
001400 SOURCE-COMPUTER.                                                 PPWWUBRC
001500 COPY  CPOTXUCS.                                                  PPWWUBRC
001600 DATA DIVISION.                                                   PPWWUBRC
001700 WORKING-STORAGE SECTION.                                         PPWWUBRC
001800                                                                  PPWWUBRC
001900 01  WS-MISC.                                                     PPWWUBRC
002000     05 WS-PGM-NAME     PIC X(08) VALUE 'PPWWUBRC'.               PPWWUBRC
002100     05 WS-GTN-NO       PIC X(03) VALUE SPACES.                   PPWWUBRC
002200                                                                  PPWWUBRC
002300 01  PPDB2MSG-INTERFACE.                                          PPWWUBRC
002400     COPY CPLNKDB2.                                               PPWWUBRC
002500                                                                  PPWWUBRC
002600******************************************************************PPWWUBRC
002700*          SQL - WORKING STORAGE                                 *PPWWUBRC
002800******************************************************************PPWWUBRC
002900                                                                  PPWWUBRC
003000 01 WS-PPPVZBRS.                                                  PPWWUBRC
003100     EXEC SQL                                                     PPWWUBRC
003200       INCLUDE PPPVZBRS                                           PPWWUBRC
003300     END-EXEC.                                                    PPWWUBRC
003400                                                                  PPWWUBRC
003500 01 WS-PPPVZGTN.                                                  PPWWUBRC
003600     EXEC SQL                                                     PPWWUBRC
003700       INCLUDE PPPVZGTN                                           PPWWUBRC
003800     END-EXEC.                                                    PPWWUBRC
003900                                                                  PPWWUBRC
004000     EXEC SQL                                                     PPWWUBRC
004100       INCLUDE SQLCA                                              PPWWUBRC
004200     END-EXEC.                                                    PPWWUBRC
004300                                                                  PPWWUBRC
004400 LINKAGE SECTION.                                                 PPWWUBRC
004500 01 PPWWUBRC-EMP-ID       PIC X(09).                              PPWWUBRC
004600 01 PPWWUBRC-BENEFIT-PLAN PIC XX.                                 PPWWUBRC
004700 01 PPWWUBRC-BENEFIT-TYPE PIC X.                                  PPWWUBRC
004800 01 PPWWUBRC-BRSC         PIC X(5).                               PPWWUBRC
004900                                                                  PPWWUBRC
005000 PROCEDURE DIVISION USING PPWWUBRC-EMP-ID                         PPWWUBRC
005100                          PPWWUBRC-BENEFIT-PLAN                   PPWWUBRC
005200                          PPWWUBRC-BENEFIT-TYPE                   PPWWUBRC
005300                          PPWWUBRC-BRSC.                          PPWWUBRC
005400                                                                  PPWWUBRC
005500 0000-MAINLINE   SECTION.                                         PPWWUBRC
005600     EXEC SQL                                                     PPWWUBRC
005700       INCLUDE CPPDXE99                                           PPWWUBRC
005800     END-EXEC.                                                    PPWWUBRC
005900*                                                                 PPWWUBRC
006000     MOVE WS-PGM-NAME TO DB2MSG-PGM-ID.                           PPWWUBRC
006100     INITIALIZE PPWWUBRC-BRSC.                                    PPWWUBRC
006200*                                                                 PPWWUBRC
006300     IF PPWWUBRC-BENEFIT-PLAN = SPACES                            PPWWUBRC
006400        EXEC SQL                                                  PPWWUBRC
006500          SELECT GTN_NUMBER                                       PPWWUBRC
006600            INTO :WS-GTN-NO                                       PPWWUBRC
006700            FROM PPPVZGTN_GTN                                     PPWWUBRC
006800           WHERE GTN_BENEFIT_TYPE = :PPWWUBRC-BENEFIT-TYPE        PPWWUBRC
006900             AND GTN_TYPE        <> 'C'                           PPWWUBRC
007000        END-EXEC                                                  PPWWUBRC
007100     ELSE                                                         PPWWUBRC
007200        EXEC SQL                                                  PPWWUBRC
007300          SELECT GTN_NUMBER                                       PPWWUBRC
007400            INTO :WS-GTN-NO                                       PPWWUBRC
007500            FROM PPPVZGTN_GTN                                     PPWWUBRC
007600           WHERE GTN_BENEFIT_PLAN = :PPWWUBRC-BENEFIT-PLAN        PPWWUBRC
007700             AND GTN_BENEFIT_TYPE = :PPWWUBRC-BENEFIT-TYPE        PPWWUBRC
007800             AND GTN_TYPE        <> 'C'                           PPWWUBRC
007900        END-EXEC                                                  PPWWUBRC
008000     END-IF                                                       PPWWUBRC
008100                                                                  PPWWUBRC
008200     IF SQLCODE = +0                                              PPWWUBRC
008300       EXEC SQL                                                   PPWWUBRC
008400           SELECT                                                 PPWWUBRC
008500            EMPLOYEE_ID,                                          PPWWUBRC
008600            BENRATE_GTN,                                          PPWWUBRC
008700            BENRATE_TUC,                                          PPWWUBRC
008800            BENRATE_REP_CODE,                                     PPWWUBRC
008900            BENRATE_SPCL_HNDLG,                                   PPWWUBRC
009000            BENRATE_DUC,                                          PPWWUBRC
009100            DED_EFF_DATE                                          PPWWUBRC
009200           INTO                                                   PPWWUBRC
009300            :WS-PPPVZBRS                                          PPWWUBRC
009400           FROM PPPVZBRS_BRS                                      PPWWUBRC
009500           WHERE EMPLOYEE_ID = :PPWWUBRC-EMP-ID                   PPWWUBRC
009600             AND BENRATE_GTN = :WS-GTN-NO                         PPWWUBRC
009700       END-EXEC                                                   PPWWUBRC
009800       IF SQLCODE = +0                                            PPWWUBRC
009900         STRING BENRATE-TUC        DELIMITED BY SIZE              PPWWUBRC
010000                BENRATE-REP-CODE   DELIMITED BY SIZE              PPWWUBRC
010100                BENRATE-SPCL-HNDLG DELIMITED BY SIZE              PPWWUBRC
010200                BENRATE-DUC        DELIMITED BY SIZE              PPWWUBRC
010300           INTO PPWWUBRC-BRSC                                     PPWWUBRC
010400         END-STRING                                               PPWWUBRC
010500       END-IF                                                     PPWWUBRC
010600     END-IF.                                                      PPWWUBRC
010700                                                                  PPWWUBRC
010800     GOBACK.                                                      PPWWUBRC
010900                                                                  PPWWUBRC
011000*999999-SQL-ERROR                  SECTION.                       PPWWUBRC
011100******************************************************************PPWWUBRC
011200* COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING          *PPWWUBRC
011300******************************************************************PPWWUBRC
011400                                                                  PPWWUBRC
011500     EXEC SQL                                                     PPWWUBRC
011600          INCLUDE CPPDXP99                                        PPWWUBRC
011700     END-EXEC.                                                    PPWWUBRC
011800     GOBACK.                                                      PPWWUBRC
011900 999999-EXIT.                                                     PPWWUBRC
012000       EXIT.                                                      PPWWUBRC
