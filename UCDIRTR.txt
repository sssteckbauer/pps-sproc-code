000100*==========================================================%      UCSD0167
000200*=    PROGRAM: UCDIRTR                                    =%      UCSD0167
000300*=    CHANGE # UCSD0167     PROJ. REQUEST: RELEASE 1415   =%      UCSD0167
000400*=    NAME:  G. CHIU        MODIFICATION DATE: 9/23/03    =%      UCSD0167
000500*=                                                        =%      UCSD0167
000600*=    DESCRIPTION                                         =%      UCSD0167
000700*=    CHANGED ROUTING PREFERENCE DEFAULT TO 'E' FROM 'N'  =%      UCSD0167
000800*===========================================================%     UCSD0167
000200**************************************************************/   52131415
000300*  PROGRAM: UCDIRTR                                          */   52131415
000400*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000500*  NAME:_______MLG_______ MODIFICATION DATE:  ___06/10/02__  */   52131415
000600*  DESCRIPTION:                                              */   52131415
000700*    STORED PROCEDURE TO VALIDATE UC0DIR DATA, INVOKED BY    */   52131415
000800*    THE INSERT AND UPDATE TRIGGERS FOR THE TABLE.           */   52131415
000900*                                                            */   52131415
001000**************************************************************/   52131415
001100     SKIP1                                                        UCDIRTR
001200 IDENTIFICATION DIVISION.                                         UCDIRTR
001300 PROGRAM-ID.    UCDIRTR.                                          UCDIRTR
001400 AUTHOR.        MAXINE GERBER.                                    UCDIRTR
001500 INSTALLATION.  UCOP.                                             UCDIRTR
001600 DATE-WRITTEN.                                                    UCDIRTR
001700 DATE-COMPILED.                                                   UCDIRTR
001800 ENVIRONMENT DIVISION.                                            UCDIRTR
001900 CONFIGURATION SECTION.                                           UCDIRTR
002000 SOURCE-COMPUTER.                COPY CPOTXUCS.                   UCDIRTR
002100 OBJECT-COMPUTER.                COPY CPOTXOBJ.                   UCDIRTR
002200 INPUT-OUTPUT SECTION.                                            UCDIRTR
002300 FILE-CONTROL.                                                    UCDIRTR
002400 DATA DIVISION.                                                   UCDIRTR
002500 FILE SECTION.                                                    UCDIRTR
002600 WORKING-STORAGE SECTION.                                         UCDIRTR
002700*                                                                 UCDIRTR
002800 01  WS-COMN.                                                     UCDIRTR
002900     05  WS-START                          PIC X(27) VALUE        UCDIRTR
003000       'WORKING STORAGE STARTS HERE'.                             UCDIRTR
003010     05  WS-PGM-NAME                       PIC X(08)              UCDIRTR
003020                                         VALUE 'UCDIRTR '.        UCDIRTR
003030     05  WS-SQLCODE                        PIC 9(04).             UCDIRTR
003050     05  WS-UCASE-ADDRESS                  PIC X(50).             UCDIRTR
003400*                                                                 UCDIRTR
003440 01  PPDB2MSG-INTERFACE.                                          UCDIRTR
003450     COPY CPLNKDB2.                                               UCDIRTR
003460                                                                  UCDIRTR
003470 01  UCWSABND.                                                    UCDIRTR
003480     COPY UCWSABND.                                               UCDIRTR
003490                                                                  UCDIRTR
003491 01  UC0VZPNP-ROW.                                                UCDIRTR
003492     EXEC SQL                                                     UCDIRTR
003493       INCLUDE UC0VZPNP                                           UCDIRTR
003494     END-EXEC.                                                    UCDIRTR
003495                                                                  UCDIRTR
003496 01  UC0VZDIR-ROW.                                                UCDIRTR
003497     EXEC SQL                                                     UCDIRTR
003498       INCLUDE UC0VZDIR                                           UCDIRTR
003499     END-EXEC.                                                    UCDIRTR
003500                                                                  UCDIRTR
003501     EXEC SQL INCLUDE SQLCA    END-EXEC.                          UCDIRTR
003502                                                                  UCDIRTR
003510 LINKAGE SECTION.                                                 UCDIRTR
003600 01 UCDIRTR-ADDRESS         PIC X(50).                            UCDIRTR
004000 01 UCDIRTR-ADDRESS-NULLIND PIC S9(9) COMP.                       UCDIRTR
004001 01 UCDIRTR-SQLSTATE        PIC X(5).                             UCDIRTR
004010 01 UCDIRTR-PROCNAM.                                              UCDIRTR
004011    05 UCDIRTR-PROCNAM-LEN  PIC 9(4) BINARY.                      UCDIRTR
004012    05 UCDIRTR-PROCNAM-TXT  PIC X(27).                            UCDIRTR
004020 01 UCDIRTR-SPECNAM.                                              UCDIRTR
004021    05 UCDIRTR-SPECNAM-LEN  PIC 9(4) BINARY.                      UCDIRTR
004022    05 UCDIRTR-SPECNAM-TXT  PIC X(18).                            UCDIRTR
004030 01 UCDIRTR-MESSAGE.                                              UCDIRTR
004040    05 UCDIRTR-MESSAGE-LEN  PIC 9(4) BINARY.                      UCDIRTR
004050    05 UCDIRTR-MESSAGE-TXT  PIC X(70).                            UCDIRTR
004100                                                                  UCDIRTR
004200 PROCEDURE DIVISION USING UCDIRTR-ADDRESS,                        UCDIRTR
004600                          UCDIRTR-ADDRESS-NULLIND,                UCDIRTR
004610                          UCDIRTR-SQLSTATE,                       UCDIRTR
004620                          UCDIRTR-PROCNAM,                        UCDIRTR
004630                          UCDIRTR-SPECNAM,                        UCDIRTR
004640                          UCDIRTR-MESSAGE.                        UCDIRTR
004700*                                                                 UCDIRTR
004800 0000-MAIN SECTION.                                               UCDIRTR
004810                                                                  UCDIRTR
005020     MOVE WS-PGM-NAME TO DB2MSG-PGM-ID.                           UCDIRTR
005021     MOVE SPACES TO UCDIRTR-MESSAGE-TXT.                          UCDIRTR
005028     MOVE ZEROES TO UCDIRTR-SQLSTATE                              UCDIRTR
005029                    UCDIRTR-MESSAGE-LEN.                          UCDIRTR
005031                                                                  UCDIRTR
005032     PERFORM 0100-EDIT-ADDRESS.                                   UCDIRTR
005033                                                                  UCDIRTR
005034     IF UCDIRTR-SQLSTATE NOT = ZERO                               UCDIRTR
005035        GO TO 0000-GOBACK                                         UCDIRTR
005036     END-IF.                                                      UCDIRTR
005062                                                                  UCDIRTR
005063     PERFORM 0200-EDIT-DATA.                                      UCDIRTR
005064                                                                  UCDIRTR
005065     IF UCDIRTR-SQLSTATE NOT = ZERO                               UCDIRTR
005066        GO TO 0000-GOBACK                                         UCDIRTR
005067     END-IF.                                                      UCDIRTR
005068                                                                  UCDIRTR
005069     PERFORM 0300-CREATE-PNP.                                     UCDIRTR
005094                                                                  UCDIRTR
005095 0000-GOBACK.                                                     UCDIRTR
005096     MOVE LENGTH OF UCDIRTR-MESSAGE-TXT TO UCDIRTR-MESSAGE-LEN    UCDIRTR
005110     GOBACK.                                                      UCDIRTR
005200                                                                  UCDIRTR
007500*                                                                 UCDIRTR
007510***************************************************************/  UCDIRTR
007520*   VALIDATE THE ADDRESS PARM PASSED                          */  UCDIRTR
007540***************************************************************/  UCDIRTR
007550 0100-EDIT-ADDRESS  SECTION.                                      UCDIRTR
007560                                                                  UCDIRTR
007575     IF UCDIRTR-ADDRESS = SPACES                                  UCDIRTR
007576     OR UCDIRTR-ADDRESS = LOW-VALUES                              UCDIRTR
007578        MOVE 'USER ID REQUIRED' TO UCDIRTR-MESSAGE-TXT            UCDIRTR
007579        MOVE '38902' TO UCDIRTR-SQLSTATE                          UCDIRTR
007580        GO TO 0100-EXIT                                           UCDIRTR
007581     END-IF.                                                      UCDIRTR
007582                                                                  UCDIRTR
007583 0100-EXIT.                                                       UCDIRTR
007590     EXIT.                                                        UCDIRTR
007591                                                                  UCDIRTR
007592     EJECT                                                        UCDIRTR
007593                                                                  UCDIRTR
007871***************************************************************/  UCDIRTR
007872*   EDIT RECORD DATA                                          */  UCDIRTR
007874***************************************************************/  UCDIRTR
007875 0200-EDIT-DATA  SECTION.                                         UCDIRTR
007876                                                                  UCDIRTR
007877     EXEC SQL                                                     UCDIRTR
007878        SELECT  UCASE(DIR_ADDRESS)                                UCDIRTR
007879               ,DIR_LAST_NAME                                     UCDIRTR
007880               ,DIR_FIRST_NAME                                    UCDIRTR
007881               ,DIR_MIDDLE_NAME                                   UCDIRTR
007882          INTO :WS-UCASE-ADDRESS                                  UCDIRTR
007883              ,:DIR-LAST-NAME                                     UCDIRTR
007884              ,:DIR-FIRST-NAME                                    UCDIRTR
007885              ,:DIR-MIDDLE-NAME                                   UCDIRTR
007886          FROM UC0VZDIR_DIR                                       UCDIRTR
007887         WHERE DIR_ADDRESS = :UCDIRTR-ADDRESS                     UCDIRTR
007888     END-EXEC.                                                    UCDIRTR
007889                                                                  UCDIRTR
007890     IF SQLCODE NOT = 0                                           UCDIRTR
007891        MOVE '38904' TO UCDIRTR-SQLSTATE                          UCDIRTR
007892        COMPUTE WS-SQLCODE = ZERO - SQLCODE                       UCDIRTR
007893        STRING                                                    UCDIRTR
007894          'SQLCODE -' DELIMITED BY SIZE                           UCDIRTR
007895          WS-SQLCODE DELIMITED BY SIZE                            UCDIRTR
007896          ' ON UC0DIR SELECT.' DELIMITED BY SIZE                  UCDIRTR
007897          INTO UCDIRTR-MESSAGE-TXT                                UCDIRTR
007898        END-STRING                                                UCDIRTR
007899        GO TO 0200-EXIT                                           UCDIRTR
007900     END-IF.                                                      UCDIRTR
007909                                                                  UCDIRTR
007910     EVALUATE TRUE                                                UCDIRTR
007911        WHEN UCDIRTR-ADDRESS NOT =  WS-UCASE-ADDRESS              UCDIRTR
007912           MOVE '38905' TO UCDIRTR-SQLSTATE                       UCDIRTR
007913           MOVE 'USERID MUST BE UPPER CASE'                       UCDIRTR
007914                                 TO UCDIRTR-MESSAGE-TXT           UCDIRTR
007915        WHEN DIR-LAST-NAME = SPACES                               UCDIRTR
007916           MOVE '38906' TO UCDIRTR-SQLSTATE                       UCDIRTR
007917           MOVE 'LAST NAME REQUIRED' TO UCDIRTR-MESSAGE-TXT       UCDIRTR
007918        WHEN DIR-FIRST-NAME = SPACES                              UCDIRTR
007919           AND DIR-MIDDLE-NAME NOT = SPACES                       UCDIRTR
007920              MOVE '38907' TO UCDIRTR-SQLSTATE                    UCDIRTR
007921              MOVE 'MIDDLE NAME W/O 1ST NAME'                     UCDIRTR
007922                                     TO UCDIRTR-MESSAGE-TXT       UCDIRTR
007923     END-EVALUATE.                                                UCDIRTR
007924                                                                  UCDIRTR
007925 0200-EXIT.                                                       UCDIRTR
007926     EXIT.                                                        UCDIRTR
007927                                                                  UCDIRTR
007928     EJECT                                                        UCDIRTR
007929***************************************************************/  UCDIRTR
007930*   IF THERE IS NOT EXISTING PNP RECORD, CREATE ONE WITH      */  UCDIRTR
007931*   ROUTING PREFERENCE SET TO N.                              */  UCDIRTR
007932***************************************************************/  UCDIRTR
007933 0300-CREATE-PNP SECTION.                                         UCDIRTR
007934                                                                  UCDIRTR
007935     PERFORM 0310-SEL-PNP.                                        UCDIRTR
007936                                                                  UCDIRTR
007937     IF SQLCODE = 100                                             UCDIRTR
007938        PERFORM 0320-INSERT-PNP                                   UCDIRTR
007939        STRING 'PNP CREATED' DELIMITED BY SIZE                    UCDIRTR
007940          INTO UCDIRTR-MESSAGE-TXT                                UCDIRTR
007941        END-STRING                                                UCDIRTR
007942        MOVE '00000' TO UCDIRTR-SQLSTATE                          UCDIRTR
007943     ELSE                                                         UCDIRTR
007944        STRING 'PNP ALREADY EXISTS' DELIMITED BY SIZE             UCDIRTR
007945          INTO UCDIRTR-MESSAGE-TXT                                UCDIRTR
007946        END-STRING                                                UCDIRTR
007947        MOVE '00000' TO UCDIRTR-SQLSTATE                          UCDIRTR
007948     END-IF.                                                      UCDIRTR
007949                                                                  UCDIRTR
007950     IF SQLCODE < 0                                               UCDIRTR
007951        MOVE '38904' TO UCDIRTR-SQLSTATE                          UCDIRTR
007952        COMPUTE WS-SQLCODE = ZERO - SQLCODE                       UCDIRTR
007953        STRING                                                    UCDIRTR
007954          'SQLCODE = -' DELIMITED BY SIZE                         UCDIRTR
007955          WS-SQLCODE DELIMITED BY SIZE                            UCDIRTR
007956          ' ON UC0PNP SELECT' DELIMITED BY SIZE                   UCDIRTR
007957          INTO UCDIRTR-MESSAGE-TXT                                UCDIRTR
007958        END-STRING                                                UCDIRTR
007959     END-IF.                                                      UCDIRTR
007960                                                                  UCDIRTR
007961 0300-EXIT.                                                       UCDIRTR
007962     EXIT.                                                        UCDIRTR
007963                                                                  UCDIRTR
007964     EJECT                                                        UCDIRTR
007965                                                                  UCDIRTR
007966***************************************************************/  UCDIRTR
007967*   SELECT ANY EXISTING PNP RECORD FOR THE ADDRESS.           */  UCDIRTR
007969***************************************************************/  UCDIRTR
007970 0310-SEL-PNP                      SECTION.                       UCDIRTR
007971                                                                  UCDIRTR
007972     MOVE 'SELECT PNP TBL' TO DB2MSG-TAG.                         UCDIRTR
007973     EXEC SQL                                                     UCDIRTR
007974        SELECT PNP_ROUTING_CODE                                   UCDIRTR
007975          INTO :PNP-ROUTING-CODE                                  UCDIRTR
007976          FROM UC0VZPNP_PNP                                       UCDIRTR
007977         WHERE PNP_ADDRESS = :UCDIRTR-ADDRESS                     UCDIRTR
007978     END-EXEC.                                                    UCDIRTR
007979                                                                  UCDIRTR
007980 0310-EXIT.                                                       UCDIRTR
007981     EXIT.                                                        UCDIRTR
007982                                                                  UCDIRTR
007983                                                                  UCDIRTR
007984 EJECT                                                            UCDIRTR
007985***************************************************************/  UCDIRTR
007986*   INSERT NEW PNP RECORD.                                    */  UCDIRTR
007987***************************************************************/  UCDIRTR
007988 0320-INSERT-PNP                      SECTION.                    UCDIRTR
007989                                                                  UCDIRTR
007990     MOVE UCDIRTR-ADDRESS TO PNP-ADDRESS.                         UCDIRTR
007991                                                                  UCDIRTR
007992     MOVE 'INSERT PNP TBL' TO DB2MSG-TAG.                         UCDIRTR
007993     EXEC SQL                                                     UCDIRTR
007994          INSERT  INTO UC0VZPNP_PNP                               UCDIRTR
007995                  (PNP_ADDRESS                                    UCDIRTR
007996                  ,PNP_ROUTING_CODE)                              UCDIRTR
007997          VALUES (:PNP-ADDRESS                                    UCDIRTR
024200                 ,'E')                                            UCSD0167
024300*****            ,'N')                                            UCSD0167
007999     END-EXEC.                                                    UCDIRTR
008000                                                                  UCDIRTR
008001 0320-EXIT.                                                       UCDIRTR
008002     EXIT.                                                        UCDIRTR
008003                                                                  UCDIRTR
008010                                                                  UCDIRTR
008100 EJECT                                                            UCDIRTR
