000100 PROCESS OUTDD(PPS)
000200**************************************************************/   51281408
000300*  PROGRAM: PPWWUGSE                                         */   51281408
000400*  RELEASE # ___1408___   SERVICE REQUEST NO(S)____15128_____*/   51281408
000500*  NAME _____AAC_______   MODIFICATION DATE ____04/19/02_____*/   51281408
000600*  DESCRIPTION                                               */   51281408
000700*   STORED PROCEDURE TO RETRIEVE A SESSION RECORD            */   51281408
000800*                                                            */   51281408
000900*      INPUT PARAMETERS : SESSION ID                         */   51281408
001000*                         SESSION TIME (LENGTH OF TIMEOUT)   */   51281408
001100*      OUTPUT VALUES :    USER ID OF THIS SESSION            */   51281408
001200*                         (BLANK IF NO SESSION FOUND)        */   51281408
001300*                         TIMESTAMP FLAG                     */   51281408
001400*                         (SESSION EXPIRED OR NOT EXPIRED)   */   51281408
001500*                                                            */   51281408
001600**************************************************************/   51281408
001700 IDENTIFICATION DIVISION.                                         PPWWUGSE
001800 PROGRAM-ID. PPWWUGSE.                                            PPWWUGSE
001900 ENVIRONMENT DIVISION.                                            PPWWUGSE
002000 CONFIGURATION SECTION.                                           PPWWUGSE
002100 SOURCE-COMPUTER.                                                 PPWWUGSE
002200 COPY  CPOTXUCS.                                                  PPWWUGSE
002300 DATA DIVISION.                                                   PPWWUGSE
002400 WORKING-STORAGE SECTION.                                         PPWWUGSE
002500                                                                  PPWWUGSE
002600 01  WS-MISC.                                                     PPWWUGSE
002700     05 WS-PGM-NAME        PIC X(08) VALUE 'PPWWUGSE'.            PPWWUGSE
002800     05 WS-CEESECS-PGM     PIC X(08) VALUE 'CEESECS'.             PPWWUGSE
002900     05 WS-TIMESTAMP1      PIC X(26) VALUE SPACES.                PPWWUGSE
003000     05 WS-TIMESTAMP2      PIC X(26) VALUE SPACES.                PPWWUGSE
003100     05 WS-TIMESTAMP-AREA.                                        PPWWUGSE
003200        10 WS-TS-LEN       PIC S9(04) COMP VALUE 26.              PPWWUGSE
003300        10 WS-TS-WRK       PIC X(26).                             PPWWUGSE
003400     05 WS-TIMESTAMP1-SECS COMP-2.                                PPWWUGSE
003500     05 WS-TIMESTAMP2-SECS COMP-2.                                PPWWUGSE
003600     05 WS-SECS-WRK        COMP-2.                                PPWWUGSE
003700     05 WS-TIMESTAMP-DISPL PIC 9(18).                             PPWWUGSE
003800     05 WS-USERID          PIC X(8)  VALUE SPACES.                PPWWUGSE
003900     05 WS-SESSION-ID      PIC X(20).                             PPWWUGSE
004000     05 WS-INTERVAL        COMP-2.                                PPWWUGSE
004100     05 WS-CONVERT-FMT.                                           PPWWUGSE
004200        10 WS-CONVERT-LEN  PIC S9(04) COMP VALUE 23.              PPWWUGSE
004300        10 WS-CONVERT-LEN  PIC X(23)                              PPWWUGSE
004400                           VALUE 'YYYY-MM-DD-HH.MI.SS.999'.       PPWWUGSE
004500     05 WS-FC              PIC X(12).                             PPWWUGSE
004600                                                                  PPWWUGSE
004700 01  PPDB2MSG-INTERFACE.   COPY CPLNKDB2.                         PPWWUGSE
004800 01  WS-UC0VZSES.                                                 PPWWUGSE
004900     EXEC SQL                                                     PPWWUGSE
005000        INCLUDE UC0VZSES                                          PPWWUGSE
005100     END-EXEC.                                                    PPWWUGSE
005200                                                                  PPWWUGSE
005300     EXEC SQL                                                     PPWWUGSE
005400        INCLUDE SQLCA                                             PPWWUGSE
005500     END-EXEC.                                                    PPWWUGSE
005600                                                                  PPWWUGSE
005700 LINKAGE SECTION.                                                 PPWWUGSE
005800*01 PPWWUGSE-SESSION-ID      PIC X(40).                           UCSD9999
005801 01 PPWWUGSE-SESSION-ID.                                          UCSD9999
005810    05 PPWWUGSE-SESSION-ID-1-20  PIC X(20).                       UCSD9999
005820    05 FILLER                    PIC X(20).                       UCSD9999
005900 01 PPWWUGSE-SESSION-TIME    PIC 99.                              PPWWUGSE
006000 01 PPWWUGSE-USER-ID         PIC X(08).                           PPWWUGSE
006100 01 PPWWUGSE-EXPIRED-FLAG    PIC X.                               PPWWUGSE
006200    88 PPWWUGSE-SESSION-EXPIRED VALUE 'Y'.                        PPWWUGSE
006300                                                                  PPWWUGSE
006400 PROCEDURE DIVISION USING PPWWUGSE-SESSION-ID                     PPWWUGSE
006500                          PPWWUGSE-SESSION-TIME                   PPWWUGSE
006600                          PPWWUGSE-USER-ID                        PPWWUGSE
006700                          PPWWUGSE-EXPIRED-FLAG.                  PPWWUGSE
006800                                                                  PPWWUGSE
006900     EXEC SQL                                                     PPWWUGSE
007000          INCLUDE CPPDXE99                                        PPWWUGSE
007100     END-EXEC.                                                    PPWWUGSE
007200     MOVE WS-PGM-NAME TO DB2MSG-PGM-ID.                           PPWWUGSE
007300                                                                  PPWWUGSE
007400 0000-MAINLINE   SECTION.                                         PPWWUGSE
007500     MOVE SPACES TO PPWWUGSE-USER-ID                              PPWWUGSE
007600     SET PPWWUGSE-SESSION-EXPIRED TO TRUE                         PPWWUGSE
007700     MOVE 'SELECT SES' TO DB2MSG-TAG                              PPWWUGSE
007800     EXEC SQL                                                     PPWWUGSE
007900       SELECT SES_USER_ID                                         PPWWUGSE
008000             ,SES_SESSION_CREATE                                  PPWWUGSE
008100             ,CURRENT TIMESTAMP                                   PPWWUGSE
008200         INTO :WS-USERID                                          PPWWUGSE
008300             ,:WS-TIMESTAMP1                                      PPWWUGSE
008400             ,:WS-TIMESTAMP2                                      PPWWUGSE
008500         FROM UC0VZSES_SES                                        PPWWUGSE
008600********WHERE SES_SESSION_ID = :PPWWUGSE-SESSION-ID               UCSD9999
008610        WHERE SES_SESSION_ID = :PPWWUGSE-SESSION-ID-1-20          UCSD9999
008700     END-EXEC                                                     PPWWUGSE
008800                                                                  PPWWUGSE
008900     IF SQLCODE = ZERO                                            PPWWUGSE
009000        MOVE WS-USERID TO PPWWUGSE-USER-ID                        PPWWUGSE
009100        MOVE WS-TIMESTAMP1 TO WS-TS-WRK                           PPWWUGSE
009200        PERFORM 8000-CONVERT-TS-TO-SECS                           PPWWUGSE
009300        MOVE WS-SECS-WRK   TO WS-TIMESTAMP1-SECS                  PPWWUGSE
009400        MOVE WS-TIMESTAMP2 TO WS-TS-WRK                           PPWWUGSE
009500        PERFORM 8000-CONVERT-TS-TO-SECS                           PPWWUGSE
009600        MOVE WS-SECS-WRK   TO WS-TIMESTAMP2-SECS                  PPWWUGSE
009700        MULTIPLY PPWWUGSE-SESSION-TIME BY 60 GIVING WS-INTERVAL   PPWWUGSE
009800        ADD WS-INTERVAL    TO WS-TIMESTAMP1-SECS                  PPWWUGSE
009900*       CHECK EXPIRATION                                          PPWWUGSE
010000        IF WS-TIMESTAMP2-SECS > WS-TIMESTAMP1-SECS                PPWWUGSE
010100*         EXPIRED! DELETE SESSION ROW (LEAVE EXPIRED FLAG SET)    PPWWUGSE
010200            MOVE 'DELETE SES' TO DB2MSG-TAG                       PPWWUGSE
010300            EXEC SQL                                              PPWWUGSE
010400              DELETE FROM UC0VZSES_SES                            PPWWUGSE
010510********       WHERE SES_SESSION_ID = :PPWWUGSE-SESSION-ID        UCSD9999
010520               WHERE SES_SESSION_ID = :PPWWUGSE-SESSION-ID-1-20   UCSD9999
010600            END-EXEC                                              PPWWUGSE
010700        ELSE                                                      PPWWUGSE
010800*         NOT EXPIRED! CLEAR EXPIRED FLAG                         PPWWUGSE
010900            MOVE SPACES TO PPWWUGSE-EXPIRED-FLAG                  PPWWUGSE
011000        END-IF                                                    PPWWUGSE
011100     END-IF                                                       PPWWUGSE
011200                                                                  PPWWUGSE
011300     GOBACK.                                                      PPWWUGSE
011400                                                                  PPWWUGSE
011500 8000-CONVERT-TS-TO-SECS SECTION.                                 PPWWUGSE
011600     CALL WS-CEESECS-PGM                                          PPWWUGSE
011700       USING WS-TIMESTAMP-AREA,                                   PPWWUGSE
011800             WS-CONVERT-FMT,                                      PPWWUGSE
011900             WS-SECS-WRK,                                         PPWWUGSE
012000             WS-FC                                                PPWWUGSE
012100     IF WS-FC NOT = LOW-VALUES                                    PPWWUGSE
012200        MOVE ZERO TO WS-SECS-WRK                                  PPWWUGSE
012300     END-IF.                                                      PPWWUGSE
012400                                                                  PPWWUGSE
012500*999999-SQL-ERROR  SECTION.                                       PPWWUGSE
012600     EXEC SQL                                                     PPWWUGSE
012700          INCLUDE CPPDXP99                                        PPWWUGSE
012800     END-EXEC.                                                    PPWWUGSE
012900     GOBACK.                                                      PPWWUGSE
